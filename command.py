'''
    TODO: I need to write a docstring
'''
import multiprocessing
import tkinter
import tkinter.ttk
import traceback
import queue
import requests


class Application(tkinter.ttk.Frame):
    '''
        TODO: I need to write a docstring
    '''
    title = 'REST Client'

    def __init__(self, master=None):
        super().__init__(master)

        # Setup the request handler
        self.request_handler = RequestHandler()
        self.request_handler.start()

        # Setup everything else
        self.master = master

        # Add a title to the top of the window
        self.winfo_toplevel().title(self.title)

        # Create variables to bind the values of the URL text widget and
        # action combobox
        self.url = tkinter.StringVar(
            self,
            'https://cat-fact.herokuapp.com/facts?animal_type=cat,horse'
        )
        self.action = tkinter.StringVar(self)

        # Create and configure all of the widgets
        self.url_entry = tkinter.ttk.Entry(self, textvariable=self.url)
        self.action_combobox = tkinter.ttk.Combobox(
            self,
            textvariable=self.action,
            values=['GET', 'POST'],
            width=5
        )
        self.submit_button = tkinter.ttk.Button(
            self,
            text='Submit',
            command=self.submit
        )
        self.output_treeview = tkinter.ttk.Treeview(self)
        self.output_treeview_scroll = tkinter.ttk.Scrollbar(
            self,
            orient='vertical',
            command=self.output_treeview.yview
        )
        self.output_treeview.config(columns=('value', 'type'))
        self.output_treeview.heading('value', text='Value')
        self.output_treeview.heading('type', text='Type')
        self.output_treeview.config(yscroll=self.output_treeview_scroll.set)

        # Configure the layout of the widgets using the grid system
        self.grid(
            column=0,
            row=0,
            sticky=(tkinter.N, tkinter.S, tkinter.E, tkinter.W)
        )
        self.url_entry.grid(
            column=0,
            row=0,
            columnspan=2,
            sticky=(tkinter.W, tkinter.E)
        )
        self.action_combobox.grid(column=2, row=0, sticky=(tkinter.E))
        self.action_combobox.current(0)
        self.submit_button.grid(
            column=3,
            row=0,
            columnspan=2,
            sticky=(tkinter.E)
        )
        self.output_treeview.grid(
            column=0,
            row=1,
            columnspan=4,
            sticky=(tkinter.N, tkinter.S, tkinter.E, tkinter.W)
        )
        self.output_treeview_scroll.grid(
            column=4,
            row=1,
            sticky=(tkinter.N, tkinter.S, tkinter.E)
        )

        # Configure the weights for the columns and rows so the window can
        # resize properly
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.columnconfigure(2, weight=0)
        self.columnconfigure(3, weight=0)
        self.columnconfigure(4, weight=0)
        self.rowconfigure(0, weight=0)
        self.rowconfigure(1, weight=1)

        # Start the queue check "timer"
        self.check_rh_queue()

    def submit(self):
        '''
            TODO: I need to write a docstring
        '''
        self.request_handler.inqueue.put((self.action.get(), self.url.get()))

    def check_rh_queue(self):
        '''
            TODO: I need to write a docstring
        '''
        try:
            output = self.request_handler.outqueue.get_nowait()
        except queue.Empty:
            output = None

        if output:
            self.update_output_treeview(output)
        self.after(100, self.check_rh_queue)

    def update_output_treeview(self, output):
        '''
            TODO: I need to write a docstring
        '''
        self.output_treeview.delete(*self.output_treeview.get_children())
        parents = [(output, '')]

        while parents:
            parent, iid = parents.pop(0)

            for idx, item in enumerate(parent):
                if isinstance(parent, list):
                    item_type = type(item)
                    item_text = idx
                    item_val = item
                elif isinstance(parent, dict):
                    item_type = type(parent[item])
                    item_text = item
                    item_val = parent[item]
                else:
                    # This shouldn't happen ever.
                    print('Parent {} is neither a list nor a dictionary'.
                          format(iid))
                    return

                if item_type in [dict, list]:
                    new_iid = self.output_treeview.insert(
                        iid,
                        tkinter.END,
                        text=item_text
                    )
                    self.output_treeview.set(new_iid, 'type', item_type)
                    parents.append((item_val, new_iid))
                else:
                    new_iid = self.output_treeview.insert(
                        iid,
                        tkinter.END,
                        text=item
                    )
                    self.output_treeview.set(new_iid, 'value', item_val)
                    self.output_treeview.set(new_iid, 'type', type(item))


class RequestHandler(multiprocessing.Process):
    '''
        TODO: I need to write a docstring
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.inqueue = multiprocessing.Queue()
        self.outqueue = multiprocessing.Queue()

    def run(self):
        while True:
            action, url = self.inqueue.get()
            try:
                response = requests.request(action, url)
                response.raise_for_status()
                self.outqueue.put(response.json())
            # TODO: Fix this exception.  We should have a better response for
            #       bad status or bad json
            except Exception:
                self.outqueue.put(traceback.format_exc())
                continue


if __name__ == '__main__':
    ROOT = tkinter.Tk()
    ROOT.columnconfigure(0, weight=1)
    ROOT.rowconfigure(0, weight=1)
    APP = Application(master=ROOT)
    try:
        APP.mainloop()
    finally:
        APP.request_handler.terminate()
